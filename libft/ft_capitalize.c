/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_capitalize.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/02 15:00:18 by cnolte            #+#    #+#             */
/*   Updated: 2017/08/02 17:15:45 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_capitalize(char **str)
{
	size_t	i;

	i = 1;
	if (ft_isalpha((*str)[0]))
		(*str)[0] = ft_toupper((*str)[0]);
	while (i < ft_strlen((*str)))
	{
		(*str)[i] = ft_tolower((*str)[i]);
		i++;
	}
	return ((*str));
}
