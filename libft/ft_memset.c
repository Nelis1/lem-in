/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memset.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/22 11:39:41 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/07 10:24:25 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*chr;

	if (len == 0)
		return (b);
	chr = (unsigned char *)b;
	while (len--)
	{
		*chr = (unsigned char)c;
		if (len)
			chr++;
	}
	return (b);
}
