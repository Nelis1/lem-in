/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_error.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 12:46:04 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/07 14:59:18 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_putendl_error(char const *s)
{
	ft_putstr("ERROR: ");
	ft_putstr((char *)s);
	ft_putstr(" Quitting...");
	ft_putchar('\n');
	return (-1);
}
