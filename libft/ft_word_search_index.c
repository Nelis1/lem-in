/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_word_search_index.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 18:46:55 by cnolte            #+#    #+#             */
/*   Updated: 2017/11/20 18:46:56 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	word_search_index(char *string, char *search)
{
	int i;
	int	j;
	int	first;
	int	len;

	len = (int)ft_strlen(search);
	i = 0;
	first = 0;
	while (string[i])
	{
		j = 0;
		if (string[i] == search[j])
		{
			first = i;
			while (string[i] && string[i] == search[j])
			{
				i++;
				j++;
			}
		}
		if (len == j)
			return (first);
		i++;
	}
	return (0);
}
