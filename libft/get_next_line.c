/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/19 16:37:33 by cnolte            #+#    #+#             */
/*   Updated: 2017/11/19 16:37:34 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	exit_func(char **str, char ***line, char *buf, int i)
{
	char	*tmp;

	if (i == 1)
	{
		if (*str && (*str)[0] != '\0')
		{
			**line = ft_strdup(*str);
			(*str)[0] = '\0';
			return (1);
		}
	}
	else if (i == 2)
	{
		tmp = ft_strjoin(*str, buf);
		free(*str);
		*str = ft_strdup(tmp);
		free(tmp);
	}
	return (0);
}

static int	test_new_line_buf(char **buf, char **str, char ***line)
{
	char	*tmp;
	int		nl;
	int		i;

	i = 0;
	if ((ft_strchr(*buf, '\n')))
	{
		nl = ft_chrindex(*buf, '\n');
		tmp = ft_strnew(nl);
		while (--nl >= 0)
			tmp[nl] = (*buf)[nl];
		**line = ft_strjoin(*str, tmp);
		ft_strdel(str);
		ft_strdel(&tmp);
		nl = ft_chrindex(*buf, '\n');
		tmp = ft_strnew(ft_strlen(*buf) - nl);
		while ((*buf)[++nl])
			tmp[i++] = (*buf)[nl];
		*str = ft_strdup(tmp);
		ft_strdel(&tmp);
		ft_strdel(buf);
		return (1);
	}
	return (0);
}

static void	store_remain_data(char ***str)
{
	char	*tmp;
	int		nl;
	int		strlen;
	int		i;

	i = 0;
	strlen = ft_strlen(**str);
	nl = ft_chrindex(**str, '\n');
	tmp = ft_strnew(strlen - nl);
	while (++nl < strlen)
		tmp[i++] = (**str)[nl];
	ft_strdel(*str);
	**str = ft_strdup(tmp);
	ft_strdel(&tmp);
}

static int	test_new_line(char **str, char ***line)
{
	int		i;
	char	*tmp;

	if (ft_strchr(*str, '\n'))
	{
		i = ft_chrindex(*str, '\n');
		tmp = ft_strnew(i);
		while (--i >= 0)
			tmp[i] = (*str)[i];
		**line = ft_strdup(tmp);
		ft_strdel(&tmp);
		store_remain_data(&str);
		return (1);
	}
	return (0);
}

int			get_next_line(const int fd, char **line)
{
	int			ret;
	static char	*str;
	char		*buf;

	if (str && test_new_line(&str, &line))
		return (1);
	buf = ft_strnew(BUFF_SIZE);
	while (((ret = read(fd, buf, BUFF_SIZE)) > 0))
	{
		if (!str)
			str = ft_strnew(1);
		buf[ret] = '\0';
		if (test_new_line_buf(&buf, &str, &line))
			return (1);
		exit_func(&str, &line, buf, 2);
	}
	if (ret == -1)
		return (-1);
	if (exit_func(&str, &line, buf, 1))
		return (1);
	ft_strdel(&buf);
	ft_strdel(&str);
	return (0);
}
