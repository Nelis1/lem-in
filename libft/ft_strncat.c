/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 09:26:10 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/07 11:04:12 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	size_t		len;
	int			i;

	len = 0;
	i = 0;
	while (dest[len])
		len++;
	while (src[i++] && n--)
		dest[(len) + (i - 1)] = src[i - 1];
	dest[(len - 1) + i] = '\0';
	return (dest);
}
