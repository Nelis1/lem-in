/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/24 12:28:00 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/08 16:17:23 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *str, const char *search)
{
	int	i;
	int	j;

	i = 0;
	if (!(*search))
		return ((char *)str);
	while (str[i])
	{
		j = 0;
		while (search[j] == str[i + j])
		{
			if (search[j + 1] == '\0')
				return (((char *)str) + i);
			j++;
		}
		i++;
	}
	return (0);
}
