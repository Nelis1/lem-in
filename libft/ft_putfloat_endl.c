/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putfloat_endl.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 18:44:53 by cnolte            #+#    #+#             */
/*   Updated: 2017/11/20 18:44:54 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putfloat_endl(float num)
{
	int		int_val;

	if (num < 0)
	{
		num = num * -1;
		ft_putchar('-');
	}
	int_val = num;
	ft_putnbr(int_val);
	ft_putchar('.');
	int_val = (num * 1000000) - (int_val * 1000000);
	ft_putnbr(int_val);
	ft_putchar('\n');
}
