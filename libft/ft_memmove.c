/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 16:28:31 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/09 12:42:17 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	char	*t_src;
	char	*t_dest;
	size_t	i;

	i = -1;
	t_src = (char *)src;
	t_dest = (char *)dest;
	if (t_src < t_dest)
		while ((int)(--n) >= 0)
			*(t_dest + n) = *(t_src + n);
	else
		while (++i < n)
			*(t_dest + i) = *(t_src + i);
	return (dest);
}
