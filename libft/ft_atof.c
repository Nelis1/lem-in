/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/29 11:33:37 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/09 11:02:03 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static float	fraction(float nbr, const char *str, int i)
{
	int	fractions;

	fractions = 10;
	if (str[i] == '.')
	{
		i++;
		while (str[i] && (str[i] >= 48) && (str[i] <= 57))
		{
			if ((str[i] - 48) > 0)
				nbr = nbr + (((float)str[i] - 48) / fractions);
			fractions *= 10;
			i++;
		}
	}
	return (nbr);
}

float			ft_atof(const char *str)
{
	int		i;
	int		neg;
	float	nbr;

	i = 0;
	nbr = 0.0;
	neg = 0;
	while ((str[i] == '\n') || (str[i] == '\t') || (str[i] == '\v')
			|| (str[i] == ' ') || (str[i] == '\f') || (str[i] == '\r'))
		i++;
	if (str[i] == '-')
		neg = 1;
	if (str[i] == '+' || str[i] == '-')
		i++;
	while (str[i] && (str[i] >= 48) && (str[i] <= 57))
	{
		nbr = nbr * 10;
		nbr = nbr + str[i] - 48;
		i++;
	}
	nbr = fraction(nbr, str, i);
	if (neg == 1)
		return (-nbr);
	else
		return (nbr);
}
