/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 15:38:20 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/09 12:13:17 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	temp;

	i = 0;
	temp = (unsigned char)c;
	while (i++ < n)
	{
		((unsigned char *)dest)[i - 1] = ((unsigned char *)src)[i - 1];
		if (((unsigned char *)dest)[i - 1] == temp)
			return (&((unsigned char *)dest)[i]);
	}
	return (NULL);
}
