/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 10:18:40 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/07 14:54:15 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	size_t			i;
	int				temp_n;
	unsigned int	j;
	char			*nbr;

	i = 1;
	temp_n = n;
	while (temp_n /= 10)
		i++;
	j = n;
	if (n < 0)
	{
		j = -n;
		i++;
	}
	if (!(nbr = ft_strnew(i)))
		return (NULL);
	nbr[--i] = j % 10 + 48;
	while (j /= 10)
		nbr[--i] = j % 10 + 48;
	if (n < 0)
		*nbr = '-';
	return (nbr);
}
