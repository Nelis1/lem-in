/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 08:18:46 by cnolte            #+#    #+#             */
/*   Updated: 2017/06/07 11:02:42 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *dest, const char *src)
{
	size_t	len;
	int		i;

	len = 0;
	i = 0;
	while (dest[len])
		len++;
	while (src[i++])
		dest[(len) + (i - 1)] = src[i - 1];
	dest[(len - 1) + i] = '\0';
	return (dest);
}
