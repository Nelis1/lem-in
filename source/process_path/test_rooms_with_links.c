/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_start_end_with_links.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 10:32:32 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/12 10:32:50 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int	test_if_end_is_found_in_links(t_ant_info *ant_info)
{
	ant_info->lst_room.tmp = ant_info->lst_room.first;
	while (ant_info->lst_room.tmp)
	{
		if (ant_info->lst_room.tmp->end_room != NULL)
		{
			ant_info->lst_link.tmp = ant_info->lst_link.first;
			while (ant_info->lst_link.tmp)
			{
				if (ant_info->lst_link.tmp->end_link != NULL)
				{
					if (!ft_strcmp(ant_info->lst_room.tmp->end_room,
							ant_info->lst_link.tmp->end_link))
						return (1);
				}
				if (ant_info->lst_link.tmp->start_link != NULL)
				{
					if (!ft_strcmp(ant_info->lst_room.tmp->end_room,
							ant_info->lst_link.tmp->start_link))
						return (1);
				}
				ant_info->lst_link.tmp = ant_info->lst_link.tmp->next;
			}
			break ;
		}
		ant_info->lst_room.tmp = ant_info->lst_room.tmp->next;
	}
	return (0);
}

int	test_if_start_is_found_in_links(t_ant_info *ant_info)
{
	ant_info->lst_room.tmp = ant_info->lst_room.first;
	while (ant_info->lst_room.tmp)
	{
		if (ant_info->lst_room.tmp->start_room != NULL)
		{
			ant_info->lst_link.tmp = ant_info->lst_link.first;
			while (ant_info->lst_link.tmp)
			{
				if (ant_info->lst_link.tmp->start_link != NULL)
				{
					if (!ft_strcmp(ant_info->lst_room.tmp->start_room,
							ant_info->lst_link.tmp->start_link))
						return (1);
				}
				if (ant_info->lst_link.tmp->end_link != NULL)
				{
					if (!ft_strcmp(ant_info->lst_room.tmp->start_room,
							ant_info->lst_link.tmp->end_link))
						return (1);
				}
				ant_info->lst_link.tmp = ant_info->lst_link.tmp->next;
			}
			break ;
		}
		ant_info->lst_room.tmp = ant_info->lst_room.tmp->next;
	}
	return (0);
}
