/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_path.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 10:05:22 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/12 12:13:33 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

// static int	clear_path(t_lst_path *lst_path)
// {
// 	t_path	*tmp_path;

// 	lst_path->tmp = lst_path->first;
// 	while (lst_path->tmp)
// 	{
// 		ft_strdel(&lst_path->tmp->from_room);
// 		ft_strdel(&lst_path->tmp->to_room);
// 		tmp_path = lst_path->tmp;
// 		lst_path->tmp = lst_path->tmp->next;
// 		free(tmp_path);
// 		tmp_path = NULL;
// 	}
// 	lst_path->first = NULL;
// 	lst_path->list = NULL
// 	lst_path->tmp = NULL;
// 	return (-1);
// }

static t_path	*add_path(char *from_room, char *to_room)
{
	t_path	*tmp;

	tmp = (t_path *)malloc(sizeof(t_path));
	tmp->from_room = ft_strdup(from_room);
	tmp->to_room = ft_strdup(to_room);
	return (tmp);
}

void	add_path_to_lst(t_lst_path *lst_path, char *from_room, char *to_room)
{
	ft_putstr("adding: ");
	ft_putstr(from_room);
	ft_putstr("--");
	ft_putendl(to_room);

	if (lst_path->first == NULL)
	{
		lst_path->tmp = add_path(from_room, to_room);
		lst_path->list = lst_path->tmp;
		lst_path->first = lst_path->list;
	}
	else
	{
		lst_path->tmp = add_path(from_room, to_room);
		lst_path->list->next = lst_path->tmp;
		lst_path->list = lst_path->tmp;
	}
	lst_path->list->next = NULL;
}

static int check_path_his(t_lst_path *lst_path, char *from_room, char *to_room, char *start, char *end)
{
	lst_path->tmp = lst_path->first;
	while (lst_path->tmp)
	{
		if (!ft_strcmp(lst_path->tmp->from_room, from_room))
		{
			if (!ft_strcmp(lst_path->tmp->to_room, to_room))
				return (-1);
		}
		else if (!ft_strcmp(lst_path->tmp->from_room, to_room))
		{
			if (!ft_strcmp(lst_path->tmp->to_room, from_room))
				return (-1);
		}
		// else if (!ft_strcmp(lst_path->tmp->to_room, end))
		// 	return (clear_path(lst_path));
		else if (!ft_strcmp(lst_path->tmp->to_room, end))
			return (1);
		lst_path->tmp = lst_path->tmp->next;
	}
}

static int	search_for_link(t_ant_info *ant_info, char *search, char *start, char *end)
{
	ant_info->lst_link.tmp = ant_info->lst_link.first;
	while (ant_info->lst_link.tmp)
	{
		if (!ft_strcmp(search, ant_info->lst_link.tmp->start_link))
		{
			if (!check_path_his(&ant_info->lst_path, search, ant_info->lst_link.tmp->end_link, start, end))
			{
				add_path_to_lst(&ant_info->lst_path, search, ant_info->lst_link.tmp->end_link);
				search_for_link(ant_info, ant_info->lst_link.tmp->end_link, start, end);
				break ;
			}
		}
		else if (!ft_strcmp(ant_info->lst_link.tmp->end_link, search))
		{
			if (!check_path_his(&ant_info->lst_path, search, ant_info->lst_link.tmp->start_link, start, end))
			{
				add_path_to_lst(&ant_info->lst_path, search, ant_info->lst_link.tmp->start_link);
				search_for_link(ant_info, ant_info->lst_link.tmp->start_link, start, end);
				break ;
			}
		}
		ant_info->lst_link.tmp = ant_info->lst_link.tmp->next;
	}
	return (0);
}

static int	find_path(t_ant_info *ant_info)
{
	char	*tmp_start;
	char	*tmp_end;
	char	*search;
	int		tries;

	tries = 0;
	ant_info->lst_room.tmp = ant_info->lst_room.first;
	while (ant_info->lst_room.tmp)
	{
		if (ant_info->lst_room.tmp->start_room != NULL)
			tmp_start = ant_info->lst_room.tmp->start_room;
		else if (ant_info->lst_room.tmp->end_room != NULL)
			tmp_end = ant_info->lst_room.tmp->end_room;
		ant_info->lst_room.tmp = ant_info->lst_room.tmp->next;
	}
	ft_putendl(tmp_start);
	ft_putendl(tmp_end);

	search_for_link(ant_info, tmp_start, tmp_start, tmp_end);

	ft_putendl("paths:");
	ant_info->lst_path.tmp = ant_info->lst_path.first;
	while (ant_info->lst_path.tmp)
	{
		ft_putstr(ant_info->lst_path.tmp->from_room);
		ft_putstr("<->");
		ft_putendl(ant_info->lst_path.tmp->to_room);
		ant_info->lst_path.tmp = ant_info->lst_path.tmp->next;
	}
	return (-1);
	return (0);
}

static int	test_if_valid_map(t_ant_info *ant_info)
{
	if (!test_if_start_is_found_in_links(ant_info))
		return (ft_putendl_error("Start room could not be found in links."));
	if (!test_if_end_is_found_in_links(ant_info))
		return (ft_putendl_error("End room could not be found in links."));
	return (0);
}

int			process_path(t_ant_info *ant_info)
{
	if (test_if_valid_map(ant_info))
		return (-1);
	if (find_path(ant_info))
		return (-1);
	return (0);
}
