/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 18:21:38 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/12 11:30:36 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdio.h>//REMOVE!!!

static void	exit_func(t_ant_info *ant_info)
{
	t_room	*tmp_room;
	t_link	*tmp_link;
	t_path	*tmp_path;

	ant_info->lst_room.tmp = ant_info->lst_room.first;
	while (ant_info->lst_room.tmp)
	{
		ft_strdel(&ant_info->lst_room.tmp->start_room);
		ft_strdel(&ant_info->lst_room.tmp->norm_room);
		ft_strdel(&ant_info->lst_room.tmp->end_room);
		tmp_room = ant_info->lst_room.tmp;
		ant_info->lst_room.tmp = ant_info->lst_room.tmp->next;
		free(tmp_room);
		tmp_room = NULL;
	}
	ant_info->lst_link.tmp = ant_info->lst_link.first;
	while (ant_info->lst_link.tmp)
	{
		ft_strdel(&ant_info->lst_link.tmp->start_link);
		ft_strdel(&ant_info->lst_link.tmp->end_link);
		tmp_link = ant_info->lst_link.tmp;
		ant_info->lst_link.tmp = ant_info->lst_link.tmp->next;
		free(tmp_link);
		tmp_link = NULL;
	}
	ant_info->lst_path.tmp = ant_info->lst_path.first;
	while (ant_info->lst_path.tmp)
	{
		ft_strdel(&ant_info->lst_path.tmp->from_room);
		ft_strdel(&ant_info->lst_path.tmp->to_room);
		tmp_path = ant_info->lst_path.tmp;
		ant_info->lst_path.tmp = ant_info->lst_path.tmp->next;
		free(tmp_path);
		tmp_path = NULL;
	}
}

static void	init(t_ant_info *ant_info)
{
	ant_info->num_of_ants = 0;
	ant_info->valid_start_found = 0;
	ant_info->valid_end_found = 0;
	ant_info->lst_room.tmp = NULL;
	ant_info->lst_room.first = NULL;
	ant_info->lst_room.list = NULL;
	ant_info->lst_link.tmp = NULL;
	ant_info->lst_link.first = NULL;
	ant_info->lst_link.list = NULL;
	ant_info->lst_path.tmp = NULL;
	ant_info->lst_path.first = NULL;
	ant_info->lst_path.list = NULL;
}

int	main(void)
{
	t_ant_info	ant_info;

	if (isatty(STDIN_FILENO) || fcntl(0, F_GETFL) < 0)
		return (ft_putendl_error("Invalid file specified."));
	init(&ant_info);
	if (process_map(&ant_info))
	{
		exit_func(&ant_info);
		return (-1);
	}
	if (process_path(&ant_info))
	{
		exit_func(&ant_info);
		return (-1);
	}
	printf("num of ants:%d\n", ant_info.num_of_ants);
	ant_info.lst_room.tmp = ant_info.lst_room.first;
	while (ant_info.lst_room.tmp)
	{
		if (ant_info.lst_room.tmp->start_room != NULL)
			printf("Start room: %s\n", ant_info.lst_room.tmp->start_room);
		ant_info.lst_room.tmp = ant_info.lst_room.tmp->next;
	}
	ant_info.lst_room.tmp = ant_info.lst_room.first;
	while (ant_info.lst_room.tmp)
	{
		if (ant_info.lst_room.tmp->norm_room != NULL)
			printf("Room: %s\n", ant_info.lst_room.tmp->norm_room);
		ant_info.lst_room.tmp = ant_info.lst_room.tmp->next;
	}
	ant_info.lst_room.tmp = ant_info.lst_room.first;
	while (ant_info.lst_room.tmp)
	{
		if (ant_info.lst_room.tmp->end_room != NULL)
			printf("End room: %s\n", ant_info.lst_room.tmp->end_room);
		ant_info.lst_room.tmp = ant_info.lst_room.tmp->next;
	}
	ant_info.lst_link.tmp = ant_info.lst_link.first;
	while (ant_info.lst_link.tmp)
	{
		printf("Link: %s -> %s\n", ant_info.lst_link.tmp->start_link, ant_info.lst_link.tmp->end_link);
		ant_info.lst_link.tmp = ant_info.lst_link.tmp->next;
	}
	exit_func(&ant_info);
	return (0);
}
