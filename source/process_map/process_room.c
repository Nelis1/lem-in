/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_room.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 21:43:38 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 21:43:38 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void	test_room_name(int *credibility, char **line)
{
	while (**line && ft_isspace(**line))
		(*line)++;
	(*credibility)++;
	while (**line && !ft_isspace(**line))
		(*line)++;
	while (**line && ft_isspace(**line))
		(*line)++;
}

int			test_if_room(char *line, int line_num)
{
	int	credibility;

	credibility = 0;
	test_room_name(&credibility, &line);
	if (test_if_x_coord(&credibility, &line, line_num))
		return (-1);
	if (test_if_neg_or_valid_decimal_x(&line, line_num))
		return (-1);
	line++;
	if (skip_whitespace_test_y_if_digit(&line, &credibility, line_num))
		return (-1);
	if (test_if_neg_or_valid_decimal_y(&line, line_num))
		return (-1);
	line--;
	if (!ft_isdigit(*line))
		return (print_error_room_coord(4, line_num));
	line++;
	if (credibility == 3)
		return (1);
	return (0);
}
