/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_to_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 17:25:02 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/11 18:10:12 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static t_link	*add_link(char *line)
{
	t_link	*tmp;
	int		start;
	int		len;

	start = 0;
	len = 0;
	tmp = (t_link *)malloc(sizeof(t_link));
	while (line[start] && ft_isspace(line[start]))
		start++;
	while (line[start + len] && line[start + len] != '-')
		len++;
	tmp->start_link = ft_strsub(line, start, len);
	start = start + len + 1;
	len = 0;
	while (line[start + len] && !ft_isspace(line[start + len]))
		len++;
	tmp->end_link = ft_strsub(line, start, len);
	return (tmp);
}

void			add_link_to_lst(t_lst_link *lst_link, char *line)
{
	if (lst_link->first == NULL)
	{
		lst_link->tmp = add_link(line);
		lst_link->list = lst_link->tmp;
		lst_link->first = lst_link->list;
	}
	else
	{
		lst_link->tmp = add_link(line);
		lst_link->list->next = lst_link->tmp;
		lst_link->list = lst_link->tmp;
	}
	lst_link->list->next = NULL;
}

static void		add_start_or_end_room(t_ant_info *ant_info, char *line,
	t_room *tmp, int i)
{
	int		start;
	int		len;

	start = 0;
	len = 0;
	while (line[start] && ft_isspace(line[start]))
		start++;
	while (line[start + len] && !ft_isspace(line[start + len]))
		len++;
	if (i == 1)
	{
		tmp->start_room = ft_strsub(line, start, len);
		tmp->norm_room = NULL;
		tmp->end_room = NULL;
		ant_info->valid_start_found = 3;
	}
	else if (i == 2)
	{
		tmp->end_room = ft_strsub(line, start, len);
		tmp->norm_room = NULL;
		tmp->start_room = NULL;
		ant_info->valid_end_found = 3;
	}
}

static t_room	*add_room(t_ant_info *ant_info, char *line)
{
	t_room	*tmp;
	int		start;
	int		len;

	start = 0;
	len = 0;
	tmp = (t_room *)malloc(sizeof(t_room));
	while (line[start] && ft_isspace(line[start]))
		start++;
	while (line[start + len] && !ft_isspace(line[start + len]))
		len++;
	if (ant_info->valid_start_found == 2)
		add_start_or_end_room(ant_info, line, tmp, 1);
	else if (ant_info->valid_end_found == 2)
		add_start_or_end_room(ant_info, line, tmp, 2);
	else
	{
		tmp->norm_room = ft_strsub(line, start, len);
		tmp->start_room = NULL;
		tmp->end_room = NULL;
	}
	return (tmp);
}

void			add_room_to_lst(t_ant_info *ant_info, char *line)
{
	if (ant_info->lst_room.first == NULL)
	{
		ant_info->lst_room.tmp = add_room(ant_info, line);
		ant_info->lst_room.list = ant_info->lst_room.tmp;
		ant_info->lst_room.first = ant_info->lst_room.list;
	}
	else
	{
		ant_info->lst_room.tmp = add_room(ant_info, line);
		ant_info->lst_room.list->next = ant_info->lst_room.tmp;
		ant_info->lst_room.list = ant_info->lst_room.tmp;
	}
	ant_info->lst_room.list->next = NULL;
}
