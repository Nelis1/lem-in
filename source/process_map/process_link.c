/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_link.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 15:17:16 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/11 15:36:00 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int	test_if_link(char *line, int line_num)
{
	if (!ft_strchr(line, '-'))
	{
		ft_putstr("ERROR: No '-' not found for room link at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
		return (-1);
	}
	return (0);
}
