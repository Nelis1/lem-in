/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_y_coord.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 21:41:12 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 21:41:12 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int	test_decimal_y(char **line, int line_num)
{
	(*line)--;
	if (ft_isdigit(**line))
		*line += 2;
	else
		return (print_error_room_digit_y(1, line_num));
	if (ft_isdigit(**line))
		(*line)--;
	else
		return (print_error_room_digit_y(2, line_num));
	return (0);
}

int			test_if_neg_or_valid_decimal_y(char **line, int line_num)
{
	int	neg;
	int	decimal;

	neg = 0;
	decimal = 0;
	while (**line && !ft_isspace(**line))
	{
		if (**line == '-' && neg == 0)
			neg = 1;
		else if (ft_isdigit(**line))
			neg = -1;
		else if ((**line == ',' || **line == '.') && decimal == 0)
		{
			if (test_decimal_y(line, line_num))
				return (-1);
			decimal = 1;
		}
		else if (!ft_isdigit(**line))
			return (print_error_room_digit_y(3, line_num));
		(*line)++;
	}
	return (0);
}

int			skip_whitespace_test_y_if_digit(char **line, int *credibility,
				int line_num)
{
	while (**line && ft_isspace(**line))
		(*line)++;
	if (ft_atoi(*line) || ft_isdigit(**line))
		(*credibility)++;
	else
		return (print_error_room_coord(3, line_num));
	return (0);
}
