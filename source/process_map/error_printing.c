/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_printing.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 21:40:20 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/12 11:22:01 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int	err_prt_wline(char *msg, int line_num)
{
	ft_putstr("ERROR: ");
	ft_putstr(msg);
	ft_putchar(' ');
	ft_putnbr(line_num);
	ft_putendl(". Quitting...");
	return (-1);
}

int	print_error_room_digit_y(int error_code, int line_num)
{
	if (error_code == 1)
	{
		ft_putstr("ERROR: Invalid decimal value for coordinate Y at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	else if (error_code == 2)
	{
		ft_putstr("ERROR: Invalid decimal value for coordinate Y at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	else if (error_code == 3)
	{
		ft_putstr("ERROR: Room - Invalid charater at ");
		ft_putstr("coordinate Y value at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	return (-1);
}

int	print_error_room_digit_x(int error_code, int line_num)
{
	if (error_code == 1)
	{
		ft_putstr("ERROR: Invalid decimal value for coordinate X at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	else if (error_code == 2)
	{
		ft_putstr("ERROR: Invalid decimal value for coordinate X at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	else if (error_code == 3)
	{
		ft_putstr("ERROR: Room - Invalid charater at ");
		ft_putstr("coordinate X value at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	return (-1);
}

int	print_error_room_coord(int error_code, int line_num)
{
	if (error_code == 1)
	{
		ft_putstr("Room - Bad data before coordinate X at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	else if (error_code == 2)
	{
		ft_putstr("Room - Bad data after coordinate X at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	else if (error_code == 3)
	{
		ft_putstr("Room - Bad data before coordinate Y at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	else if (error_code == 4)
	{
		ft_putstr("Room - Bad data after coordinates Y at line ");
		ft_putnbr(line_num);
		ft_putendl(". Quitting...");
	}
	return (-1);
}
