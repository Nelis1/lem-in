/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_x_coord.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 21:42:25 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/09 21:42:25 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int	test_decimal_x(char **line, int line_num)
{
	(*line)--;
	if (ft_isdigit(**line))
		*line += 2;
	else
		return (print_error_room_digit_x(1, line_num));
	if (ft_isdigit(**line))
		(*line)--;
	else
		return (print_error_room_digit_x(2, line_num));
	return (0);
}

int			test_if_neg_or_valid_decimal_x(char **line, int line_num)
{
	int	neg;
	int	decimal;

	neg = 0;
	decimal = 0;
	while (**line && !ft_isspace(**line))
	{
		if (**line == '-' && neg == 0)
			neg = 1;
		else if (ft_isdigit(**line))
			neg = -1;
		else if ((**line == ',' || **line == '.') && decimal == 0)
		{
			if (test_decimal_x(line, line_num))
				return (-1);
			decimal = 1;
		}
		else if (!ft_isdigit(**line))
			return (print_error_room_digit_x(3, line_num));
		(*line)++;
	}
	(*line)--;
	if (!ft_isdigit(**line))
		return (print_error_room_coord(2, line_num));
	return (0);
}

int			test_if_x_coord(int *credibility, char **line, int line_num)
{
	if (ft_atoi(*line) || ft_isdigit(**line))
		(*credibility)++;
	else
		return (print_error_room_coord(1, line_num));
	return (0);
}
