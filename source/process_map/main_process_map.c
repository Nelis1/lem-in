/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_process_map.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/09 21:39:06 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/12 11:23:01 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int	determine_format(char *line, int line_num)
{
	if (ft_countwords(line) == 3)
	{
		if (test_if_room(line, line_num) == 1)
			return (1);
		else
			return (-1);
	}
	else if (ft_countwords(line) == 1)
	{
		if (!test_if_link(line, line_num))
			return (2);
		else
			return (-1);
	}
	else
		return (err_prt_wline("Invalid line data found at line", line_num));
	return (0);
}

static int	process_line_for_room_or_link(t_ant_info *ant_info, char *line,
				int line_num)
{
	int	format;

	format = determine_format(line, line_num);
	if (format < 0)
		return (-1);
	if (ant_info->valid_start_found == 1 && format == 1)
		ant_info->valid_start_found = 2;
	else if (ant_info->valid_start_found == 1 && format != 1)
		return (err_prt_wline("No valid room found after \"##start\" at line",
					line_num));
	else if (ant_info->valid_end_found == 1 && format == 1)
		ant_info->valid_end_found = 2;
	else if (ant_info->valid_end_found == 1 && format != 1)
		return (err_prt_wline("No valid room found after \"##end\" at line",
				line_num));
	if (format == 1)
		add_room_to_lst(ant_info, line);
	else if (format == 2)
		add_link_to_lst(&ant_info->lst_link, line);
	return (0);
}

static int	get_number_of_ants(t_ant_info *ant_info, char *line)
{
	int	i;

	i = -1;
	if (ft_countwords(line) != 1)
		return (ft_putendl_error("Spaces found in number of ants at line 1."));
	if (ft_atoi(line) < 1)
	{
		ft_putstr("ERROR: Number of ants is not a valid integer or not a ");
		ft_putendl("positive number at line 1. Quitting...");
		return (-1);
	}
	while (line[++i])
		if (!ft_isdigit(line[i]) && !ft_isspace(line[i]))
			return (err_prt_wline("Number of ants is not only numbers at", 1));
	ant_info->num_of_ants = ft_atoi(line);
	return (0);
}

int			process_map(t_ant_info *ant_info)
{
	char	*line;
	int		line_num;

	line_num = 1;
	while (get_next_line(0, &line))
	{
		if (ft_strchr(line, '#') && !ft_strstr(line, "##"))
				;
		else if (ft_strstr(line, "##") && !(ft_strstr(line, "##start") || ft_strstr(line, "##end")))
			return (err_prt_wline("\"##\" found without start or end at line", line_num));
		else if (ant_info->num_of_ants == 0)
		{
			if (get_number_of_ants(ant_info, line))
			{
				free(line);
				return (-1);
			}
		}
		else
		{
			if (ft_strstr(line, "##start"))
			{
				if (ant_info->valid_start_found > 0)
					return (err_prt_wline("Duplicate \"##start\" found at line",
							line_num));
				else
					ant_info->valid_start_found = 1;
			}
			else if (ft_strstr(line, "##end"))
			{
				if (ant_info->valid_end_found > 0)
					return (err_prt_wline("Duplicate \"##end\" found at line",
							line_num));
				else
					ant_info->valid_end_found = 1;
			}
			else if (process_line_for_room_or_link(ant_info, line, line_num))
			{
				free(line);
				return (-1);
			}
		}
		line_num++;
		free(line);
	}
	return (0);
}
