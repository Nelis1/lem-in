/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prototypes.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 17:23:19 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/12 11:31:37 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROTOTYPES_H
# define PROTOTYPES_H

int		err_prt_wline(char *msg, int line_num);
int		process_map(t_ant_info *ant_info);

int		print_error_room_digit_y(int error_code, int line_num);
int		print_error_room_digit_x(int error_code, int line_num);
int		print_error_room_coord(int error_code, int line_num);

int		test_if_room(char *line, int line_num);
int		test_if_x_coord(int *credibility, char **line, int line_num);
int		test_if_neg_or_valid_decimal_x(char **line, int line_num);
int		skip_whitespace_test_y_if_digit(char **line, int *credibility,
			int line_num);
int		test_if_neg_or_valid_decimal_y(char **line, int line_num);

int		test_if_link(char *line, int line_num);
void	add_link_to_lst(t_lst_link *lst_link, char *line);
void	add_room_to_lst(t_ant_info *ant_info, char *line);

int		process_path(t_ant_info *ant_info);
int		test_if_start_is_found_in_links(t_ant_info *ant_info);
int		test_if_end_is_found_in_links(t_ant_info *ant_info);

#endif
