/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   structs.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cnolte <cnolte@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 17:23:23 by cnolte            #+#    #+#             */
/*   Updated: 2018/01/11 17:23:24 by cnolte           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCTS_H
# define STRUCTS_H

typedef struct	s_links
{
	char			*start_link;
	char			*end_link;
	struct s_links	*next;
}				t_link;

typedef struct	s_lst_link
{
	t_link	*first;
	t_link	*list;
	t_link	*tmp;
}				t_lst_link;

typedef struct	s_room
{
	char			*norm_room;
	char			*start_room;
	char			*end_room;
	struct s_room	*next;
}				t_room;

typedef struct	s_lst_room
{
	t_room	*first;
	t_room	*list;
	t_room	*tmp;
}				t_lst_room;

typedef struct	s_path
{
	char			*from_room;
	char			*to_room;
	struct s_path	*next;
}				t_path;

typedef struct	s_lst_path
{
	t_path	*first;
	t_path	*list;
	t_path	*tmp;
}				t_lst_path;

typedef struct	s_ant_info
{
	int			num_of_ants;
	int			valid_start_found;
	int			valid_end_found;
	t_lst_room	lst_room;
	t_lst_link	lst_link;
	t_lst_path	lst_path;
}				t_ant_info;

#endif
