NAME = lem-in

CC = gcc #-Wall -Werror -Wextra

CFLAGS = -o $(NAME) -I libft/ -L libft/ -I include/

CFILES = source/main.c
CFILES += source/process_map/error_printing.c		\
			source/process_map/main_process_map.c	\
			source/process_map/process_room.c		\
			source/process_map/process_x_coord.c	\
			source/process_map/process_y_coord.c	\
			source/process_map/process_link.c		\
			source/process_map/add_to_list.c		\
			source/process_path/process_path.c		\
			source/process_path/test_rooms_with_links.c

CFILES += libft/libft.a

$(NAME):
	$(MAKE) -C libft all
	$(CC) $(CFILES) $(CFLAGS)

all: $(NAME)

clean:
	$(MAKE) -C libft clean

fclean:
	$(MAKE) -C libft fclean
	rm -rf $(NAME)

re: fclean all

co:
	rm -rf $(NAME)
	$(CC) $(CFILES) $(CFLAGS)

.PHONY: co re fclean clean all
